# Exercise 1: Dockerize Your Node.js App

In this exercise, you will configure your Node.js application to be built as a Docker image and run it in a container.

## 1. Create Dockerfile

1. Create a `Dockerfile` in the root folder of your Node.js project with the following content:

```Dockerfile
# Use the official Node.js image with a specific version
FROM node:14

# Create a directory for your app inside the container
RUN mkdir -p /usr/src/app

# Copy the application files to the container
COPY . /usr/src/app

# Set the working directory to the app directory
WORKDIR /usr/src/app

# Expose the port your app will run on
EXPOSE 3000

# Install the app's dependencies
RUN npm install

# Start the Node.js application
CMD ["node", "server.js"]
```

This Dockerfile sets up the environment for your Node.js app, copies your application files, installs dependencies, and defines how to start your app.

2. Build the Docker Image
Navigate to the directory containing your Dockerfile and run the following command to build the Docker image:

```bash
docker build -t {image-name}:{tag} .
```

Replace {image-name} with the desired name for your Docker image and {tag} with a version or tag for the image (e.g., my-node-app:1.0).

## 3. Run the Docker Container
Once the Docker image is built, you can run it in a container using the following command:

```bash
docker run -p 3000:3000 -d {image-name}:{tag}
```

Replace {image-name} and {tag} with the same values used during image building.

Your Node.js app should now be running in a Docker container, and you can access it on port 3000.

By completing this exercise, you've Dockerized your Node.js application, making it easier to deploy and manage in containerized environments.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-automation-and-cicd-with-jenkins/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
