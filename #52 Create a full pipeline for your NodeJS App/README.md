# Exercise 2: Create a Full Pipeline for Your Node.js App

In this exercise, you will create a complete Jenkins pipeline for your Node.js application. The pipeline will include the following steps:

1. Increment version: The application's version and Docker image version will be incremented.
2. Run tests: Ensure that the code is tested, and the pipeline will abort if tests fail.
3. Build a Docker image with the incremented version.
4. Push the Docker image to a Docker repository.
5. Commit the version update to a Git repository.

## Prerequisites

Before setting up the pipeline, ensure the following:

- Jenkins is installed and configured.
- Docker is installed on the Jenkins server.
- Jenkins credentials for Docker and Git repositories are created:
  - Create `usernamePassword` credentials for the Docker registry called `docker-credentials`.
  - Create `usernamePassword` credentials for the Git repository called `gitlab-credentials`.

## Configure Node Tool in Jenkins Configuration

1. In Jenkins, go to "Manage Jenkins" > "Global Tool Configuration."
2. Under the "NodeJS" section, configure a NodeJS tool named "node" (this is how it's referenced in the Jenkinsfile).

## Jenkinsfile

Create a `Jenkinsfile` in your project's root directory with the following content:

```groovy
pipeline {
    agent any
    tools {
        nodejs "node"
    }
    stages {
        stage('Increment version') {
            steps {
                script {
                    dir("app") {
                        sh "npm version minor"  // Update application version (minor or patch)
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"  // Set the new version as part of IMAGE_NAME
                    }
                }
            }
        }
        stage('Run tests') {
            steps {
               script {
                    dir("app") {
                        sh "npm install"
                        sh "npm run test"
                    } 
               }
            }
        }
        stage('Build and Push Docker image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]){
                    sh "docker build -t docker-hub-id/myapp:${IMAGE_NAME} ."
                    sh 'echo $PASS | docker login -u $USER --password-stdin'
                    sh "docker push docker-hub-id/myapp:${IMAGE_NAME}"
                }
            }
        }
        stage('Commit version update') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'
                        sh 'git remote set-url origin https://$USER:$PASS@https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/jenkins-exercises.git'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:jenkins-jobs'
                    }
                }
            }
        }
    }
}
```

This Jenkinsfile defines the complete pipeline for your Node.js application. It increments the version, runs tests, builds and pushes a Docker image, and commits the version update to Git. Ensure to replace placeholders like docker-hub-id with your Docker registry and adapt other settings to your environment.

## Running the Pipeline
Set up Jenkins as a CI/CD server.
Create a new Jenkins job that uses this Jenkinsfile.
Run the Jenkins job, and it will execute the complete pipeline.
By completing this exercise, you've set up a robust pipeline for your Node.js application, ensuring continuous integration and delivery.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-automation-and-cicd-with-jenkins/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
