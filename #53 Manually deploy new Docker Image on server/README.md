# EXERCISE 3: Manually Deploy New Docker Image on Server

After the pipeline has run successfully, you can manually deploy the new Docker image on your server. Follow these steps:

1. SSH into your droplet server using your private key:

```shell
ssh -i ~/id_rsa root@{server-ip-address}
```

Log in to your Docker Hub registry:

```bash
docker login
```

Pull and run the new Docker image from the registry. Replace {docker-hub-id}, {image-name}, and other parameters as needed:

```bash
docker run -p 3000:3000 {docker-hub-id}/myapp:{image-name}
```

By following these steps, you can manually deploy the latest Docker image of your Node.js application on your server.

# Contributions
Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/build-automation-and-cicd-with-jenkins/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
