# EXERCISE 4: Extract into Jenkins Shared Library

In this exercise, you will extract the logic from your Jenkinsfile into a Jenkins Shared Library to make it reusable and shareable across projects. Follow these steps:

1. Create a separate Git repository for your Jenkins Shared Library. This repository will contain the shared pipeline logic.

2. Extract the code within the `script` blocks from the following stages in your Jenkinsfile:
   - Increment version
   - Run tests
   - Build and Push Docker image
   - Commit version update

   Place these code blocks into separate Groovy scripts within your Jenkins Shared Library repository.

3. Configure your Jenkinsfile to use the Jenkins Shared Library project by adding the appropriate reference.

For detailed guidance on these steps, refer to the demo videos in the module, as they explain the process in detail.

To validate that your code is correct, execute the pipeline when you're done. If you obtain the same result as before and have a new image in the registry at the end of the pipeline execution, then you have successfully completed the exercise.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/build-automation-and-cicd-with-jenkins/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
